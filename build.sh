#!/bin/bash

set -o errexit
set -o notify
set -o nounset
set -o pipefail

readonly __DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __ROOT__="$(cd "$(dirname "${__DIR__}")" && pwd)"
readonly __SELF__="${__DIR__}/$(basename "${BASH_SOURCE[0]}")"
readonly __BASE__="$(basename "${__SELF__}" .sh)"

exception()
{
  echo "An error occurred. Exiting."
}

__main()
{
  trap exception \
    ERR SIGINT SIGTERM SIGQUIT

  if [ "$#" -ne 7 ]; then
    printf "Wrong number of arguments\n"
    printf "Usage: <build_number> <profile_directory> <drupal_root> <db_user> <db_pass> <db_host> <db_name>\n"
    exit 1
  fi

  local build_number="$1"
  local profile_directory="$2"
  local drupal_root="$3"
  local db_user="$4"
  local db_pass="$5"
  local db_host="$6"
  local db_name="$7"

  # ==============================================================================
  #
  # VARIABLES
  #
  # ==============================================================================

  local profile=$(basename ${profile_directory})

  local db_url="mysql://${db_user}:${db_pass}@${db_host}/${db_name}"

  local site_name="cds-${build_number}"
  local site_mail="drupalbot@wwu.edu"

  local account_name="admin"
  local account_pass="admin${build_number}"
  local account_mail="${site_mail}"

  local user_name="auth"
  local user_pass="auth${build_number}"
  local user_mail="noreply@wwu.edu"

  # ==============================================================================
  #
  # SETUP
  #
  # ==============================================================================

  rm \
    --preserve-root \
    --force \
    --recursive \
    --verbose \
    ${drupal_root}

  # ==============================================================================
  #
  # DRUSH MAKE
  #
  # ==============================================================================

  drush make ${profile_directory}/${profile}.make ${drupal_root}

  cp -R ${profile_directory} ${drupal_root}/profiles/

  # ==============================================================================
  #
  # DRUSH SITE-INSTALL
  #
  # ==============================================================================

  cd $drupal_root

  drush site-install ${profile} \
    --yes \
    --db-url=${db_url} \
    --site-name=${site_name} \
    --site-mail=${site_mail} \
    --account-mail=${account_mail} \
    --account-name=${account_name} \
    --account-pass=${account_pass} \
    || true

  drush user-create ${user_name} \
    --password=${user_pass} \
    --mail=${user_mail} \
    || true

  # ==============================================================================
  #
  # WWUZEN THEME COMPILE
  #
  # ==============================================================================

  cd ${drupal_root}/sites/all/themes/wwuzen

  bundle install

  npm install

  grunt build

  cd $drupal_root

  # ==============================================================================
  #
  # SITES/DEFAULT FILE PERMISSIONS
  #
  # ==============================================================================

  chmod --recursive 0777 ${drupal_root}/sites/default

  # ==============================================================================
  #
  # JQUERY UPDATE
  #
  # ==============================================================================

  drush vset --yes jquery_update_jquery_cdn "google"

  drush vset --yes jquery_update_jquery_version "1.7"

  # ==============================================================================
  #
  # DISABLE CLIENT-SIDE ADMIN MENU CACHE
  #
  # ==============================================================================

  drush vset --yes admin_menu_cache_client "0"
}

__main $@
